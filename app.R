#
# This is a Shiny web application. You can run the application by clicking
# the 'Run App' button above.
#
# Find out more about building applications with Shiny here:
#
#    http://shiny.rstudio.com/
#   test

library(shiny)
library(DT)
setwd(dirname(getwd()))





# Define UI for application that draws a histogram
ui <- navbarPage(
  tags$style(type="text/css", "body {padding-top: 10px;}"),
  
  
  tabPanel("Differential Expression",
   
   # Application title
    
   
   # Sidebar with a slider input for number of bins 
   sidebarLayout(
     sidebarPanel(
       selectInput(inputId = "cancer_type",
                   label = "Cancer Type",
                   choices = NULL),
       
       
       selectInput(inputId = "design_matrix_column",
                   label = "Select a cohort column from the phenotype table",
                   choices = c("NORMAL vs TUMOR","AMONG CANCER types"),
                   selected = "NORMAL vs TUMOR"),
       
       checkboxInput("GTEX_flag","GTEX only")
     ),
      
      # Show a plot of the generated distribution
        mainPanel(
          tabsetPanel(
            tabPanel(title = "Differential Expression Results",DT::dataTableOutput("diff_ex_table"))
          )
          
      )
   )

  ),
  tabPanel("Single Gene Analysis",
           sidebarLayout(
             sidebarPanel(
               textInput(inputId = 'gene_id',label = 'Enter a Gene Name')
               
             ),
           
           mainPanel(
             plotlyOutput(outputId = 'plot_comparison')
           )
           
           )
  
   )
)

# Define server logic required to draw a histogram
server <- function(input, output, session) {
  #gene <- reactive({input$gene_id})
  #analysis <- reactive({input$design_matrix_column})
  #cancer <- reactive({input$cancer_type})
  
  source("~/TCGA_cygnal_app/cygnal_gepia/Algorithms/Differential_expression.R")
  source("~/TCGA_cygnal_app/cygnal_gepia/Algorithms/Mapping.R")
  source("~/TCGA_cygnal_app/cygnal_gepia/Algorithms/single_gene_plot.R")
  
  
  proto_cnt <- read.delim("/home/pranay/TCGA_cygnal_app/proto/testing_app_proto_cnt.txt",sep=" ")
  proto_phen <-read.delim("/home/pranay/TCGA_cygnal_app/proto/testing_app_proto_phen.txt",sep=" ")
  proto_phen$cancer_type <- sapply(proto_phen$primary.disease.or.tissue,mapper)
  
  updateSelectInput(session, "cancer_type",choices = unique(proto_phen$cancer_type),selected = "KIRC")
  
  
  output$plot_comparison <-renderPlotly({
    single_gene_plot(count_mat=proto_cnt,phenotype_mat=proto_phen,selected_gene=input$gene_id)  
  }) 
  
  
  #result_df <-diff_ex_t_test(count_mat = proto_cnt,phenotype_mat = proto_phen,analysis_type = analysis(),analysis_cancer = cancer()) 
  
  output$diff_ex_table <- DT::renderDataTable({
    result_df <-diff_ex_t_test(count_mat = proto_cnt,phenotype_mat = proto_phen,analysis_type = input$design_matrix_column,analysis_cancer = input$cancer_type,GTEX = input$GTEX_flag)
  })
   
  
}

# Run the application 
shinyApp(ui = ui, server = server)

