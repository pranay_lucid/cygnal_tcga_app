

single_gene_plot <- function(count_mat,phenotype_mat,selected_gene){
  colnames(count_mat) <-  gsub(".","-",colnames(count_mat),fixed = T)
  count_mat <- count_mat[selected_gene,]
  cancers <- c(unique(phenotype_mat$cancer_type))
  normal_samples <- list()
  tumor_samples <- list()
  for (cancer in cancers){
    normal_samples[[cancer]] <- phenotype_mat[phenotype_mat$normal_or_tumor=="NORMAL" & phenotype_mat$cancer_type==cancer,]$sample
    tumor_samples[[cancer]] <- phenotype_mat[phenotype_mat$normal_or_tumor=="TUMOR" & phenotype_mat$cancer_type==cancer,]$sample
    
  }
  normal_vals <- list()
  tumor_vals <- list()
  for (cancer in cancers){
    normal_vals[[cancer]] <- count_mat[colnames(count_mat) %in% normal_samples[[cancer]]]
    tumor_vals[[cancer]] <- count_mat[colnames(count_mat) %in% tumor_samples[[cancer]]]
  }
  normal_vals_mean <- unlist(lapply(normal_vals,function(x) mean(t(x))))
  tumor_vals_mean <- unlist(lapply(tumor_vals,function(x) mean(t(x))))
  df_plot <- data.frame(cancers,normal_vals_mean,tumor_vals_mean)
  
  p <- plot_ly(data = df_plot,x=~cancers,y=~normal_vals_mean,type='bar',name="Normal") %>% add_trace(y=~tumor_vals_mean,name="Tumor") %>% layout(yaxis=list(title="count"),barmode="group")
  
  return(p)
}
